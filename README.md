# This is an example repositoy of a team
As a developer, I constantly update the Dockerfile with the changes I want to apply to my docker image.

When I finish a ros package, I add it to this repository in the following path: `$HOME/competition-team-example/ws/src` and then I push it.


Everyday, the first thing I do before starting developing is:
```bash
cd $HOME/competition-team-example/
git pull --rebase
docker pull registry.gitlab.com/daniel.lopez.puig/competition-team-example
<path_to_pal_docker_utils>/pal_docker.sh -it -v /dev/snd:/dev/snd -v /$HOME/ws/src:/home/user/ws registry.gitlab.com/daniel.lopez.puig/competition-team-example
# Open terminator
terminator -u &
```
Then, working inside the docker I place my files at `/home/user/ws` that actually is stored in my pc inside `$HOME/ws/src`. This way next day I will keep my changes no matter the changes on the docker image.

If I changed the Dockerfile or I want to share a new package with my mates I would like to push my changes to this repository and [update the team docker image](How-to-update-the-team-docker-image)
## How to update the team docker image
There are 2 main reason to do it:
- As a team lead, when PAL notifies me there is a new docker image
- As a team member, I want to "deploy" my changes to our docker image

```bash
#Download the latest competition docker image, note you have to substitute <X.X>
docker login registry.gitlab.com
docker pull registry.gitlab.com/competitions4/sciroc/dockers/sciroc:<X.X>
# Update your local private_gitlab containing the latest Dockerfile
cd $HOME/competition-team-example/
git pull --rebase
# Build your image from competition
docker build .
# Tag the pulled docker with your personal docker name
docker tag registry.gitlab.com/competitions4/sciroc/dockers/sciroc:<X.X> registry.gitlab.com/daniel.lopez.puig/competition-team-example
# Upload your new image to private_gitlab
docker push registry.gitlab.com/daniel.lopez.puig/competition-team-example
```
